# This example has been replaced by the [Firebase Queue](https://github.com/firebase/firebase-queue) NPM module

Please check out the new repository for a more comprehensive, robust framework on which to build a task queue powered by Firebase
